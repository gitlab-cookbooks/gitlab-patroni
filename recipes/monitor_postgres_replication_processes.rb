# Cookbook:: gitlab-patroni
# Recipe:: monitor_postgres_replication_processes
# License:: MIT
#
# Copyright:: 2024, GitLab Inc.
#
# Periodically poll the Postgres walsender, walreceiver, and startup (wal applier) processes
# to measure how close they are to saturation.  Write the resulting prometheus metrics
# to node_exporter's file-based metrics directory.

script_path = node['gitlab-patroni']['monitor_postgres_replication_processes']['script']
samples_per_run = node['gitlab-patroni']['monitor_postgres_replication_processes']['samples_per_run']
sampling_duration_seconds = node['gitlab-patroni']['monitor_postgres_replication_processes']['sampling_duration_seconds']
output_file = node['gitlab-patroni']['monitor_postgres_replication_processes']['output_file']

cookbook_file script_path do
  source 'poll_process_states_for_postgres_streaming_replication.pl'
  owner node['gitlab-patroni']['monitor_postgres_replication_processes']['unix_user']
  group node['gitlab-patroni']['monitor_postgres_replication_processes']['unix_group']
  mode '0755'
end

monitor_postgres_replication_processes_service_unit = {
  Unit: {
    Description: 'Timer-run service to sample process states for Postgres walsender, walreceiver, and startup processes',
  },
  Service: {
    Type: 'oneshot',
    ExecStart: "#{script_path} --samples=#{samples_per_run} --duration=#{sampling_duration_seconds} --outfile=#{output_file}",
    TimeoutStartSec: node['gitlab-patroni']['monitor_postgres_replication_processes']['max_oneshot_duration_seconds'],
    User: node['gitlab-patroni']['monitor_postgres_replication_processes']['unix_user'],
  },
}

monitor_postgres_replication_processes_timer_unit = {
  Unit: {
    Description: 'Timer to generate metrics for Postgres walsender, walreceiver, and startup processes',
  },
  Timer: {
    OnCalendar: node['gitlab-patroni']['monitor_postgres_replication_processes']['systemd_timer']['on_calendar'],
    AccuracySec: '1s',
  },
  Install: {
    WantedBy: 'timers.target',
  },
}

systemd_unit 'monitor_postgres_replication_processes.service' do
  content monitor_postgres_replication_processes_service_unit
  action [:create, :enable]
end

running_action = node['gitlab-patroni']['monitor_postgres_replication_processes']['systemd_timer']['enabled'] ? :start : :stop
enabled_action = node['gitlab-patroni']['monitor_postgres_replication_processes']['systemd_timer']['enabled'] ? :enable : :disable

systemd_unit 'monitor_postgres_replication_processes.timer' do
  content monitor_postgres_replication_processes_timer_unit
  action [:create, enabled_action, running_action]
end
