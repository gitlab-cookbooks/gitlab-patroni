#!/usr/bin/perl

# Purpose:
# Emit Prometheus metrics measuring how often the Postgres streaming replication processes spend
# in each process state (running on CPU, waiting for disk IO, or idle).
#
# Implementation notes:
# Periodically sample each process's state, as reported by kernel via `/proc/[PID]/status`.
# After collecting N samples (polled every M milliseconds), emit the aggregated results as
# as each process's proportion of samples in each process state.  This approximates the
# proportion of wallclock time spent in those states.
#
# Background on Postgres replication:
# Each Postgres replication stream involves 3 single-threaded processes:
#  * `walsender`: Reads WAL records from local cached files and writes them to a TCP socket to `walreceiver`.
#  * `walreceiver`: Reads WAL records from TCP socket and writes them to local WAL files.
#    It calls `fdatasync` often, causing foreground write latency to affect replication throughput.
#  * `startup`: Applies WAL records.  It reads the WAL files written by `walreceiver` and applies those
#    block-level changes to the Postgres data files.  Depending on filesystem cache state, it may become
#    sensitive to disk read latency, but its writes are typically cached and asynchronously flushed.
#
# For more background on motivation and goals, see the research summary in:
# https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2892

use strict;

# Only use standard modules (https://perldoc.perl.org/modules#Standard-Modules) that are installed by default.
use Time::HiRes qw/usleep/;
use Getopt::Long qw/GetOptions/;

our $NUM_SAMPLES = 60;
our $SAMPLING_DURATION_SECONDS = 15;
our $SAMPLING_INTERVAL_SECONDS = $SAMPLING_DURATION_SECONDS / $NUM_SAMPLES;
our $OUTPUT_FILENAME;

# These process states are the relevant subset from kernel's `task_state_array` in `fs/proc/array.c`,
# which is used by `get_task_state` to render the "State: [code] [description]" line of `/proc/[PID]/status`.
our $possible_process_states = {
    'R' => 'running',
    'S' => 'sleeping',
    'D' => 'disk wait',
    'T' => 'stopped by signal',
    't' => 'stopped by tracing',
    'Z' => 'zombie',
};

main();

sub main
{
    configure();
    my $counts_by_pid_state = poll_processes();
    report_metrics($counts_by_pid_state);
}

sub configure
{
    GetOptions(
        'help|?'     => \my $show_help,
        'samples=i'  => \$NUM_SAMPLES,
        'duration=i' => \$SAMPLING_DURATION_SECONDS,
        'outfile=s'  => \$OUTPUT_FILENAME,
    ) or usage();
    usage() if $show_help;
}

sub usage
{
    die <<~HERE;
      Usage: $0 [options]

      Options:
        --help                This help message.
        --samples [count]     (default: $NUM_SAMPLES)  Sets how many samples to capture from each process.
        --duration [seconds]  (default: $SAMPLING_DURATION_SECONDS)  Sets how many seconds to spread the sampling over.
        --outfile [filename]  Write output to [filename] instead of STDOUT.

      Purpose:
      Emit Prometheus metrics measuring how often the Postgres streaming replication processes spend
      in each process state (running on CPU, waiting for disk IO, or idle).
      HERE
}

sub poll_processes
{
    my $counts_by_pid_state = {};
    if (my @pids = find_target_processes()) {
        foreach my $i (1 .. $NUM_SAMPLES) {
            poll_processes_once(\@pids, $counts_by_pid_state);
            usleep($SAMPLING_INTERVAL_SECONDS * 1_000_000);
        }
    }
    return $counts_by_pid_state;
}

# Rather than query Postgres (requires auth), we parse the relevant label values from the process command line string.
# Example command line strings for each of the 3 postgres processes involved in streaming replication:
#     postgres: toy_pg_cluster_name: walsender replication-user 10.0.7.101(52108) streaming EFDF7/6158D038
#     postgres: toy_pg_cluster_name: walreceiver streaming EFDF7/5D59A590
#     postgres: toy_pg_cluster_name: startup recovering 00000002000EFDF70000005D
# Note: The 2nd field (cluster_name) is optional and may not always be configured, so we tolerate its absence.
use constant POSTGRES_PROCESS_CMDLINE_PATTERN => '^postgres: (\S+: )?(walsender|walreceiver|startup) ';

sub find_target_processes
{
    # Returns empty array if no matching processes exist.
    return split /\n/, qx{pgrep -f "@{[ POSTGRES_PROCESS_CMDLINE_PATTERN ]}"};
}

sub poll_processes_once
{
    my ($pids, $counts_by_pid_state) = @_;
    foreach my $pid (@$pids) {
        my $state = get_process_state_for_pid($pid);
        $counts_by_pid_state->{$pid}{$state}++;
    }
}

sub get_process_state_for_pid
{
    my ($pid) = @_;
    my $state_line = qx{grep '^State:' /proc/$pid/status};
    my ($state) = ($state_line =~ /^State:\s+(\S)/) or die "ERROR: Cannot parse state for PID $pid.";
    return $state;
}

sub report_metrics
{
    my ($counts_by_pid_state) = @_;

    return unless (keys %$counts_by_pid_state > 0);
    if ($OUTPUT_FILENAME) {
        # Reopen STDOUT.  Implicitly truncates previous run's output, so we wait to open until we are ready to write fresh results.
        open(STDOUT, '>', $OUTPUT_FILENAME) or die "ERROR: Cannot open file '$OUTPUT_FILENAME': $!";
    }
    report_metric_last_sampled_timestamp();
    report_metric_process_state_ratio($counts_by_pid_state);
}

sub report_metric_last_sampled_timestamp
{
    my $metric_name = 'postgres_replication_process_state_last_sampled_timestamp_seconds';
    print_metric_headers(
        $metric_name,
        'gauge',
        'Timestamp when last sampled this host\'s Postgres streaming replication processes (walsender, walreceiver, or startup).'
    );
    my $epoch_seconds = time();
    print_metric_value($metric_name, "%d", $epoch_seconds);
}

# NOTE: Why do we need to report zero values?
# Here we emit metrics for all possible states, intentionally including zero-valued ones.
# This correctly distinguishes betweeen missing data versus the real signal of a measured zero value.
# Reporting real zero-value measurements allows queries across time to:
#  * correctly compute the statistical distribution of metric values for each label value (e.g. percentiles, average)
#  * correctly downsample, generating an aggregated output value from higher-resolution input samples
#  * correctly deduplicate scrapes by multiple prometheus servers
# Generally if we did not report zero values, then at query time, an unreported zero value during one scrape
# would be treated like a missing value.  That can cause a nearby scrape's non-zero value to be used instead,
# which mixes measurements from different timestamps, distorting the results (e.g. ratios that do not sum to 100%).
# This effect of treating unreported zeros as missing values tends to be more prominent for gauge metrics,
# especially ones that frequently drop back down to zero.
sub report_metric_process_state_ratio
{
    my ($counts_by_pid_state) = @_;

    my $metric_name = 'postgres_replication_process_state_ratio';
    print_metric_headers(
        $metric_name,
        'gauge',
        'Proportion of time spent in each process state (idle, running, disk wait) by each Postgres replication process (walsender, walreceiver, or startup).'
    );

    foreach my $pid (sort keys %$counts_by_pid_state) {
        my $counts_by_state = $counts_by_pid_state->{$pid};
        foreach my $state (sort keys %$possible_process_states) {
            my $ratio = ( $counts_by_state->{$state} // 0 ) / $NUM_SAMPLES;
            print_metric_value($metric_name, "%0.4f", $ratio, {
                postgres_pid => $pid,
                process_state => $state,
                process_state_name => describe_process_state($state),
                extract_labels_from_postgres_process_cmdline($pid),
            });
        }
    }
}

sub print_metric_headers
{
    my ($metric_name, $type, $description) = @_;
    print <<HERE;
# HELP $metric_name $description
# TYPE $metric_name $type
HERE
}

sub print_metric_value
{
    my ($metric_name, $value_format, $metric_value, $optional_labels) = @_;
    $optional_labels ||= {};
    my $format_string = "%s";
    $format_string .= '{' . join(',', map {'%s="%s"'} sort keys %$optional_labels) . '}' if keys %$optional_labels > 0;
    $format_string .= " $value_format\n";
    printf($format_string,
        $metric_name,
        (map { $_ => $optional_labels->{$_} } sort keys %$optional_labels),
        $metric_value,
    );
}

sub extract_labels_from_postgres_process_cmdline
{
    my ($pid) = @_;
    my $cmdline_str = qx{cat /proc/$pid/cmdline} or die "ERROR: Cannot read cmdline for PID $pid\n";
    my @fields = split /\s+/, $cmdline_str;
    die "ERROR: PID $pid cmdline does not begin with 'postgres:': $cmdline_str\n" unless shift(@fields) eq 'postgres:';

    my %labels;
    ($labels{'cluster_name'}) = map {/(.*):$/} shift(@fields) if $fields[0] =~ /:$/;
    $labels{'process_type'} = shift @fields;

    if ($labels{'process_type'} eq 'walsender') {
    my ($replica_ip, $replica_port) = ($fields[1] =~ /([0-9a-fA-F.:]+)\((\d+)\)/);
    my $replica_host = qx{dig +short -x $replica_ip};
    $replica_host =~ s/\.\n// if defined $replica_host;
        $labels{'replica_ip'} = $replica_ip;
        $labels{'replica_port'} = $replica_port;
        $labels{'replica_host'} = $replica_host if defined $replica_host;
    }

    return %labels;
}

sub describe_process_state
{
    my ($state) = @_;
    return $possible_process_states->{$state} || 'unknown';
}
