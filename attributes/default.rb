default['gitlab-patroni']['user'] = 'postgres'
default['gitlab-patroni']['group'] = 'postgres'
default['gitlab-patroni']['db_name'] = 'gitlabhq_production'

default['gitlab-patroni']['secrets']['backend'] = 'dummy'
default['gitlab-patroni']['secrets']['path']['path'] = 'gitlab-gstg-secrets/gitlab-patroni'
default['gitlab-patroni']['secrets']['path']['item'] = 'gstg.enc'
default['gitlab-patroni']['secrets']['key']['ring'] = 'gitlab-secrets'
default['gitlab-patroni']['secrets']['key']['key'] = 'gstg'
default['gitlab-patroni']['secrets']['key']['location'] = 'global'

default['gitlab-patroni']['postgresql']['version'] = '14'
default['gitlab-patroni']['postgresql']['install_debug_package'] = true
default['gitlab-patroni']['postgresql']['dbg_debug_package'] = true
default['gitlab-patroni']['postgresql']['config_directory'] = '/var/opt/gitlab/postgresql'
default['gitlab-patroni']['postgresql']['data_directory'] = '/var/opt/gitlab/postgresql/data'
default['gitlab-patroni']['postgresql']['log_directory'] = '/var/log/gitlab/postgresql'
default['gitlab-patroni']['postgresql']['bin_directory'] = '/usr/lib/postgresql/14/bin'
default['gitlab-patroni']['postgresql']['listen_address'] = '0.0.0.0:5432'
default['gitlab-patroni']['postgresql']['packages'] = []
default['gitlab-patroni']['postgresql']['pgpass_hosts'] = []
default['gitlab-patroni']['postgresql']['ssl_ca'] = 'in vault'
default['gitlab-patroni']['postgresql']['ssl_cert'] = 'in vault'
default['gitlab-patroni']['postgresql']['ssl_key'] = 'in vault'
default['gitlab-patroni']['postgresql']['semmni'] = 1024
default['gitlab-patroni']['postgresql']['semmns'] = 100_000
default['gitlab-patroni']['postgresql']['shmall'] = 30_146_560
default['gitlab-patroni']['postgresql']['shmmax'] = 123_480_309_760
default['gitlab-patroni']['postgresql']['semmsl'] = 250
default['gitlab-patroni']['postgresql']['semopm'] = 32
default['gitlab-patroni']['postgresql']['parameters']['port'] = 5432
default['gitlab-patroni']['postgresql']['parameters']['ssl'] = 'off'
default['gitlab-patroni']['postgresql']['parameters']['ssl_ciphers'] = 'HIGH:MEDIUM:+3DES:!aNULL:!SSLv3:!TLSv1'
default['gitlab-patroni']['postgresql']['parameters']['log_destination'] = 'syslog'
default['gitlab-patroni']['postgresql']['remove_packages'] = []
default['gitlab-patroni']['postgresql']['use_wal_disk'] = false
default['gitlab-patroni']['postgresql']['wal_directory'] = '/var/opt/gitlab-wal/pg_wal'
default['gitlab-patroni']['postgresql']['basebackup']['waldir'] = ''

default['gitlab-patroni']['patroni']['version'] = '1.5.0'
default['gitlab-patroni']['patroni']['python_runtime_version'] = '3'
default['gitlab-patroni']['patroni']['python_package_name'] =
  if node['platform_version'].to_i > 20
    'python3.8'
  else
    'python3'
  end
default['gitlab-patroni']['patroni']['pip_version'] = '18.0'
default['gitlab-patroni']['patroni']['get_pip_url'] = 'https://bootstrap.pypa.io/pip/3.5/get-pip.py'
default['gitlab-patroni']['patroni']['psycopg2_version'] = '2.8.5'
default['gitlab-patroni']['patroni']['pg_activity_version'] = '1.6.2'
default['gitlab-patroni']['patroni']['certifi_version'] = '2021.10.8'
# This attribute can be dropped once Patroni is upgraded to 3.3.0 or higher.
# See https://github.com/zalando/patroni/blob/634b44ee0586f063033074806a42b534a354cbff/docs/releases.rst#L16.
default['gitlab-patroni']['patroni']['ydiff_version'] = '1.2'
default['gitlab-patroni']['patroni']['config_directory'] = '/var/opt/gitlab/patroni'
default['gitlab-patroni']['patroni']['install_directory'] = '/opt/patroni'
default['gitlab-patroni']['patroni']['log_directory'] = '/var/log/gitlab/patroni'
default['gitlab-patroni']['patroni']['bind_interface'] = 'lo'
default['gitlab-patroni']['patroni']['use_custom_scripts'] = false

default['gitlab-patroni']['patroni']['consul']['check_interval'] = '10s'
default['gitlab-patroni']['patroni']['consul']['service_name'] = 'DummyPatroniConsulService'
default['gitlab-patroni']['patroni']['consul']['extra_service_name'] = 'DummyPatroniConsulService'
default['gitlab-patroni']['patroni']['consul']['extra_checks']['master'] = []
default['gitlab-patroni']['patroni']['consul']['extra_checks']['replica'] = []

default['gitlab-patroni']['patroni']['users']['superuser']['username'] = 'gitlab-superuser'
default['gitlab-patroni']['patroni']['users']['superuser']['password'] = 'in-vault'
default['gitlab-patroni']['patroni']['users']['superuser']['options'] = %w[createrole createdb]
default['gitlab-patroni']['patroni']['users']['replication']['username'] = 'gitlab-replicator'
default['gitlab-patroni']['patroni']['users']['replication']['password'] = 'in-vault'
default['gitlab-patroni']['patroni']['users']['replication']['options'] = %w[replication]

default['gitlab-patroni']['patroni']['users']['console-admin']['username'] = 'console-admin'
default['gitlab-patroni']['patroni']['users']['console-admin']['password'] = ''
default['gitlab-patroni']['patroni']['users']['console-admin']['options'] = %w[superuser createrole createdb]
default['gitlab-patroni']['patroni']['users']['console-rw']['username'] = 'console-rw'
default['gitlab-patroni']['patroni']['users']['console-rw']['password'] = ''
default['gitlab-patroni']['patroni']['users']['console-ro']['username'] = 'console-ro'
default['gitlab-patroni']['patroni']['users']['console-ro']['password'] = ''

default['gitlab-patroni']['patroni']['config']['scope'] = 'pg-ha-cluster'
default['gitlab-patroni']['patroni']['config']['name'] = node.name
default['gitlab-patroni']['patroni']['config']['restapi']['listen'] = '0.0.0.0:8009'
default['gitlab-patroni']['patroni']['config']['consul']['host'] = '127.0.0.1:8500'

default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['ttl'] = 30
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['loop_wait'] = 10
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['retry_timeout'] = 10
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['maximum_lag_on_failover'] = 1_048_576
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['postgresql']['use_pg_rewind'] = true
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['postgresql']['use_slots'] = true
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['postgresql']['parameters']['wal_level'] = 'replica'
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['postgresql']['parameters']['hot_standby'] = 'on'
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['postgresql']['parameters']['wal_keep_segments'] = 8
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['postgresql']['parameters']['max_wal_senders'] = 5
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['postgresql']['parameters']['max_replication_slots'] = 5
default['gitlab-patroni']['patroni']['config']['bootstrap']['dcs']['postgresql']['parameters']['checkpoint_timeout'] = 30
default['gitlab-patroni']['patroni']['config']['bootstrap']['initdb'] = [{ 'encoding' => 'UTF8' }, { 'locale' => 'C.UTF-8' }]
default['gitlab-patroni']['patroni']['config']['bootstrap']['pg_hba'] = []
default['gitlab-patroni']['patroni']['config']['tags'] = {}

default['gitlab-patroni']['snapshot']['gcs_credentials'] = 'in vault'
default['gitlab-patroni']['snapshot']['cron']['minute'] = 0
default['gitlab-patroni']['snapshot']['cron']['hour'] = '*/6'
default['gitlab-patroni']['snapshot']['cron']['user'] = node['gitlab-patroni']['user']
default['gitlab-patroni']['snapshot']['gcs_credentials_path'] = '/etc/gitlab/gcs-snapshot.json'
default['gitlab-patroni']['snapshot']['snapshot_script_path'] = '/usr/local/bin/gcs-snapshot.sh'
default['gitlab-patroni']['snapshot']['log_path_prefix'] = '/var/log/gitlab/postgresql/gcs-snapshot'
default['gitlab-patroni']['snapshot']['suspend-during-snapshot'] = false
default['gitlab-patroni']['snapshot']['use-instant-snap-with-cg'] = false

default['gitlab-patroni']['analyze']['cron']['minute'] = 0
default['gitlab-patroni']['analyze']['cron']['hour'] = '*/12'
default['gitlab-patroni']['analyze']['analyze_script_path'] = '/usr/local/bin/analyze-namespaces-table.sh'
default['gitlab-patroni']['analyze']['log_path_prefix'] = '/var/log/gitlab/postgresql/analyze-namespaces-table'

default['gitlab-patroni']['issues_notes']['cron']['minute'] = 0
default['gitlab-patroni']['issues_notes']['cron']['hour'] = '*/1'
default['gitlab-patroni']['issues_notes']['analyze_script_path'] = '/usr/local/bin/analyze-issues_notes-table.sh'
default['gitlab-patroni']['issues_notes']['log_path_prefix'] = '/var/log/gitlab/postgresql/analyze-issues_notes-table'

default['gitlab-patroni']['zlonk']['enabled'] = true
default['gitlab-patroni']['zlonk']['directory'] = '/var/opt/gitlab/postgresql/opt/zlonk'
default['gitlab-patroni']['zlonk']['log_directory'] = '/var/log/gitlab/zlonk'
default['gitlab-patroni']['zlonk']['git-http'] = 'https://gitlab.com/gitlab-com/gl-infra/zlonk.git'
default['gitlab-patroni']['zlonk']['branch'] = 'master'
default['gitlab-patroni']['zlonk']['project'] = ''
default['gitlab-patroni']['zlonk']['instance'] = ''

default['gitlab-patroni']['patroni']['systemd']['auto_restart'] = 'no'

default['gitlab-patroni']['pg_wait_sampling']['pg_wait_sampling_reset_path'] = '/usr/local/bin/pg_wait_sampling_reset.sh'
default['gitlab-patroni']['pg_wait_sampling']['log_path_prefix'] = '/var/log/gitlab/postgresql/pg_wait_sampling'
default['gitlab-patroni']['pg_wait_sampling']['reset_cron']['minute'] = 0
default['gitlab-patroni']['pg_wait_sampling']['reset_cron']['hour'] = '*'
default['gitlab-patroni']['pg_wait_sampling']['reset_cron']['weekday'] = '*'
default['gitlab-patroni']['pg_wait_sampling']['reset_cron']['user'] = node['gitlab-patroni']['user']

default['gitlab-patroni']['monitor_postgres_replication_processes']['script'] = '/usr/local/bin/poll_process_states_for_postgres_streaming_replication.pl'
default['gitlab-patroni']['monitor_postgres_replication_processes']['samples_per_run'] = 60
default['gitlab-patroni']['monitor_postgres_replication_processes']['sampling_duration_seconds'] = 15
default['gitlab-patroni']['monitor_postgres_replication_processes']['output_file'] = '/opt/prometheus/node_exporter/metrics/postgres_replication_processes.prom'
default['gitlab-patroni']['monitor_postgres_replication_processes']['unix_user'] = 'prometheus'
default['gitlab-patroni']['monitor_postgres_replication_processes']['unix_group'] = 'prometheus'
default['gitlab-patroni']['monitor_postgres_replication_processes']['systemd_timer']['enabled'] = true
# NOTE: To validate a systemd time specification, run: `systemd-analyze calendar "<time_format_string>"`
default['gitlab-patroni']['monitor_postgres_replication_processes']['systemd_timer']['on_calendar'] = '*-*-* *:*:00,20,40'
default['gitlab-patroni']['monitor_postgres_replication_processes']['max_oneshot_duration_seconds'] = 60
