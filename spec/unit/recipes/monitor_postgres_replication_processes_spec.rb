# Cookbook:: gitlab-patroni
# Spec::monitor_postgres_replication_processes
#
# Copyright:: 2023, MIT

require 'spec_helper'

describe 'gitlab-patroni::monitor_postgres_replication_processes' do
  context 'when all attributes are default, on Ubuntu 20.04' do
    platform 'ubuntu', '20.04'

    normal_attributes['gce']['project']['projectId'] = 'spec_project'

    before do
      allow_any_instance_of(Chef::Node).to receive(:environment)
        .and_return('spec')
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates a systemd service unit and timer unit' do
      expect(chef_run).to create_systemd_unit('monitor_postgres_replication_processes.service')
      expect(chef_run).to enable_systemd_unit('monitor_postgres_replication_processes.timer')
    end

    it 'deploys script' do
      expect(chef_run).to create_cookbook_file('/usr/local/bin/poll_process_states_for_postgres_streaming_replication.pl').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755'
      )
    end
  end
end
